
<!--**********************************
    Footer start
***********************************-->
<div class="footer">

    <div class="copyright">
        <p>Copyright © Designed &amp; Developed by <a href="https://dexignlab.com/" target="_blank">DexignLab</a> 2022</p>
    </div>
</div>
<!--**********************************
    Footer end
***********************************-->
	</div>
    <!--**********************************
        Main wrapper end
    ***********************************-->

    <!--**********************************
        Scripts
    ***********************************-->
    <!-- Required vendors -->
    <script src="assets/template/vendor/global/global.min.js"></script>
	<script src="assets/template/vendor/chart.js/Chart.bundle.min.js"></script>
	<script src="assets/template/vendor/jquery-nice-select/js/jquery.nice-select.min.js"></script>

	<!-- Apex Chart -->
	<script src="assets/template/vendor/apexchart/apexchart.js"></script>
	<script src="assets/template/vendor/nouislider/nouislider.min.js"></script>
	<script src="assets/template/vendor/wnumb/wNumb.js"></script>

	<!-- Dashboard 1 -->
	<script src="assets/template/js/dashboard/dashboard-1.js"></script>

    <script src="assets/template/js/custom.min.js"></script>
	<script src="assets/template/js/dlabnav-init.js"></script>
	<script src="assets/template/js/demo.js"></script>
    <script src="assets/template/js/styleSwitcher.js"></script>

    <script type="text/javascript"></script>
<!-- <script>
    function deleteFunction() {
    event.preventDefault(); // prevent form submit
    var form = event.target.form; // storing the form
            swal({
    title: "Yakin ingin menghapus?",
    text: "Tapi kamu masih bisa membatalkannya.",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Ya, Hapus!",
    cancelButtonText: "Tidak!",
    closeOnConfirm: false,
    closeOnCancel: false
    },
    function(isConfirm){
    if (isConfirm) {
        form.submit();          // submitting the form when user press yes
    } else {
        swal("Cancelled", "Your imaginary file is safe :)", "error");
    }
    });
    }
</script> -->


</body>
</html>


